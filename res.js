'use strict';
const log = require('log4js');
log.configure({
    appenders: { login: { type: "file", filename: "login.log" } },
    categories: { default: { appenders: ["login"], level: "error" } }
});

const logger = log.getLogger('login');

exports.send = function(username,success, message, resRows, res=null) {
    var data = {
        'success': success,
        'message': message,
        'result': resRows
    };

    console.log(JSON.stringify(resRows));

    this.logging(username,new Date()," --> " + JSON.stringify(data));

    res.json(data);

    res.end();
};

exports.logging = function (username,time,log,type=null){
    if(type === null){
        logger.level = "info";
        logger.info("{'username':'"+username+"','log':'"+log+"'}");
    }
}