'use strict';

var response = require('./res');
var MDInfoAbsen = require('./model/model_info_absen');
var MDResult = require('./model/model_result');
var MDGetCheat = require('./model/model_get_cheat');
var connection = require('./conn');

exports.index = function(req, res) {
    response.send("",false,"Bad Credentials",{}, res);
};

//////////////////////////////////////////////  GET PREPARE MAPS LOCATION ///////////////////////////////////////

var global_username;
exports.getPrepareAbsen = function (req, res){
    const { headers } = req;
    if(headers['x-token-access'].length > 0){
        let token = headers['x-token-access'];
        console.log(token);
        MDInfoAbsen.getSessionInfo(token).then((infoSession)=>{
            if(infoSession) {
                console.log("--> getSession");
                global_username = infoSession.nip;
                MDInfoAbsen.getInfoPrepareAbsen(global_username).then((result) => {
                    if(result.success){
                        response.send(result.username,true,"Berhasil mendapatkan data",MDResult.getResult(),res);
                    }else{
                        response.send(result.username,false,"Gagal mendapatkan data.",{},res);
                    }
                }).catch((e)=>{
                    response.send(global_username,false,e,{},res);
                });
            }else{
                response.send(global_username,false,"Bad Credentials.",{},res);
            }
        }).catch((e)=>{
            response.send("",false,"Session tidak ditemukan.",{},res);
        });


    }else{
        response.send("",false,"Bad Credentials.",{},res);
    }
}