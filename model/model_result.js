'use strict';

var result;
exports.setResult = function (response){
    result = {
        nama_satuan_organisasi: response.nama_satuan_organisasi,
        latitude_kantor: response.latitude_kantor,
        longitude_kantor: response.longitude_kantor,
        radius_kantor: response.radius_kantor,
        pola_info: {
            nama_pola: response.nama_pola,
            open_absen: response.open_absen,
            close_absen: response.close_absen,
            jam_masuk: response.jam_masuk,
            jam_pulang: response.jam_pulang
        },
        button_masuk:response.button_masuk,
        button_wfh: response.wfh_on?true:false,
        button_wfo: response.wfo_on?true:false,
        button_pulang: response.button_pulang
    }
}

exports.getResult = function (){
    return result;
}