'use strict';

var response = require('../res');
var connection = require('../conn');
var MDResult = require('./model_result');
const request = require('request');
var moment = require('moment');

exports.getSessionInfo = function (token=null,callback=null){
    return new Promise((resolve,reject) =>{
        var query = `SELECT * from maps_user_session where token='`+token+`'`;

        connection.query(query, (err,result)=>{
            if(err){
                return reject("Error DB");
            }else{
                if(result.length > 0){
                    return resolve(result[0]);
                }else {
                    return reject("Session Not Found");
                }
            }
        });
    });
}

exports.getInfoPegawai = function (nip=null,callback=null){
    return new Promise((resolve,reject) =>{
        var query = `SELECT * from maps_pegawai where nip='`+nip+`'`;

        connection.query(query, (err,result)=>{
            if(err){
                console.log(err);
            }else{
                if(result.length > 0){
                    return resolve(result[0]);
                }else {
                    return false;
                }

            }
        });
    });
}

exports.getInfoPola = function (id_pola=null,hari_number=null,callback=null){
    return new Promise((resolve,reject) =>{
        var query = `SELECT * from maps_pola_jadwal where id_pola='` + id_pola + `' and hari_number='`+hari_number+`'`;

        connection.query(query, (err,result)=>{
            if(err){
                console.log(err);
            }else{
                if(result.length > 0){
                    return resolve(result[0]);
                }else {
                    return false;
                }

            }
        });
    });
}

exports.getInfoPrepareAbsen = function (nip=null){
    return new Promise((resolve,reject) =>{
        var dayNumber = new Date().getDay()+1;
        var query = `   SELECT 
                            s.nama_satuan AS nama_satuan_organisasi,
                            s.latitude AS latitude_kantor,
                            s.longitude AS longitude_kantor,
                            s.radius AS radius_kantor,
                            po.nama_pola,
                            pj.open_absen,
                            pj.close_absen,
                            pj.jam_masuk,
                            pj.jam_pulang, 
                            p.wfh_on,
                            p.wfo_on
                        FROM 
                            maps_pegawai p
                            left JOIN maps_pola_satuan_organisasi s ON p.satuanorganisasiid=s.kode
                            JOIN maps_pola po ON p.pola=po.id
                            left JOIN maps_pola_jadwal pj ON po.id=pj.id_pola AND pj.hari_number=${dayNumber}
                        WHERE 
                            p.nip='${nip}'`;

        connection.query(query, (err,result)=>{
            if(err){
                return reject("[Error] Prepare Absen");
            }else{
                if(result.length > 0){
                    var now = new Date();
                    var curTime = moment().format('HH:mm:ss');

                    if(result[0].open_absen === null){
                        return reject("Tidak ada absen untuk hari ini");
                    }else{
                        if(curTime > result[0].open_absen && curTime < result[0].close_absen){

                            if(curTime > result[0].jam_pulang && curTime < result[0].close_absen){
                                console.log("Hanya dapat absen Pulang");
                                result[0].button_masuk = false;
                                result[0].button_pulang = true;
                            }else{
                                console.log("Dapat absen");
                                result[0].button_masuk = true;
                                result[0].button_pulang = true;
                            }

                        }else{
                            console.log("Diluar waktu absen.");
                            result[0].button_masuk = false;
                            result[0].button_pulang = false;
                        }
                    }


                    MDResult.setResult(result[0]);
                    return resolve({
                        success:true,
                        username:result[0].nip
                    });
                }else {
                    return reject("Data not found");
                }

            }
        });
    });
}

function getLastAbsen(nip=null){
    return new Promise((resolve, reject)=>{
        var queryCheckAbsen = "select * from maps_absen where date(insert_date) = '"+ moment().format('YYYY-MM-DD') +"' and nip='"+nip+"'";
        connection.query(queryCheckAbsen,(err,result)=>{
            if(err){
                return reject("Err Database");
            }else{
                return resolve({
                    success:true,
                    count:result.length
                });
            }
        });
    });

}